# Ejercicion de Ficheros #

En este proyecto podemos varias aplicaciones relacionadas con ficheros y servidores.

### Agenda ###
Una aplicación que pemita añadir a una agenda los nombres, telégonos y emails de nuestros amigos. 
Los datos se guardan en un fichero en memoria interna.

### Alarmas ###
Permite programar varias alarmas que se escribiran en un fichero en memoria externa. Las alarmas se lanzaran de forma consecutiva.

### Dias lectivos ###
Una aplicación que calcula los días lectivos en el calendario escolar del curso actual.

### Web ###
Una aplicación que pide una dirección web y muestre en pantalla la página indicada en esa dirección. Se puede elegir entre java.net, Android Asynchronous Http Client o Volley para realizar la conexión con el servidor web.

### Imagenes ###
Una aplicación que permite ver una secuencia de imágenes descargadas de Internet, escritas en un fichero en un servidor.

### Conversor Moneda V2 ###
Segunda version de la aplicacion Conversor Mondeda que lee el cambio desde un servidor.

### Subir Ficheros ###
Una aplicación que permite elegir un fichero usando algún explorador de archivos instalado en el dispositivo y lo sube a la carpeta uploads situada en el servidor web del propio equipo o en Internet.
